from db import AutoVOQueries
from fastapi import Depends, FastAPI
from models import AutoVO


app = FastAPI()

"""
this is just a normal endpoint that lists all of the
AutoVOs that the sales service has
"""
@app.get("/api/autos", response_model=list[AutoVO])
def get_autos(queries: AutoVOQueries=Depends()):
    autos = queries.get_all_autos()
    return autos

@app.post("/api/autos")
def create_auto(
    auto: AutoVO,
    queries: AutoVOQueries=Depends()
):
    # auto = {}
    queries.create_autovo(auto)    

